#!/bin/bash

export AWS_DEFAULT_REGION=eu-west-1

ASGNAME="app-server-asg"

rm -rf /var/tmp/new-ips.txt
rm -rf /etc/haproxy/haproxy.cfg-newips

INSTACEIDS=$(aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name ${ASGNAME} | jq .AutoScalingGroups[0].Instances[].InstanceId | tr -d '"')
echo ${INSTACEIDS}

for i in $(echo ${INSTACEIDS})
  do
    aws ec2 describe-instances --instance-ids $i | jq .Reservations[].Instances[].PrivateIpAddress | tr -d '"' >> /var/tmp/new-ips.txt
done

cp /etc/haproxy/haproxy.cfg-template /etc/haproxy/haproxy.cfg-newips

for i in $(cat /var/tmp/new-ips.txt | sort)
 do
   echo "   server "srv-$(echo $i | md5sum | awk '{print $1}')" $i:80 check" >> /etc/haproxy/haproxy.cfg-newips 
done

diff /etc/haproxy/haproxy.cfg-newips /etc/haproxy/haproxy.cfg

RESULT=$?

if [ $RESULT -eq 0 ]; then
  echo "No changes."
else
  echo "Cheching syntax..."
  /usr/sbin/haproxy -c -V -f /etc/haproxy/haproxy.cfg-newips
  RESULT=$?

  if [ $RESULT -eq 0 ]; then
    echo "Change config files and reload HAproxy."
    mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg-OLD
    mv /etc/haproxy/haproxy.cfg-newips /etc/haproxy/haproxy.cfg
    systemctl reload haproxy
  else
    echo "Error in HAproxy New config file."
  fi

fi

